﻿using System;
using System.IO;
using System.Linq;

namespace FileTypeHelper
{
    public class FileTypeHelper
    {
        private static readonly byte[] BMP = { 66, 77 };
        private static readonly byte[] DOC = { 208, 207, 17, 224, 161, 177, 26, 225 };
        private static readonly byte[] EXE_DLL = { 77, 90 };
        private static readonly byte[] GIF = { 71, 73, 70, 56 };
        private static readonly byte[] ICO = { 0, 0, 1, 0 };
        private static readonly byte[] JPG = { 255, 216, 255 };
        private static readonly byte[] PDF = { 37, 80, 68, 70, 45, 49, 46 };
        private static readonly byte[] PNG = { 137, 80, 78, 71, 13, 10, 26, 10, 0, 0, 0, 13, 73, 72, 68, 82 };
        private static readonly byte[] RAR = { 82, 97, 114, 33, 26, 7, 0 };
        private static readonly byte[] TIFF = { 73, 73, 42, 0 };
        private static readonly byte[] ZIP_DOCX = { 80, 75, 3, 4 };
        private static readonly byte[] TXT_UTF8 = {239, 187, 191};
        private static readonly byte[] TXT_UTF16LE = {255, 254};
        private static readonly byte[] TXT_UTF16BE = {254, 255};
        private static readonly byte[] TXT_UTF32BE = {0, 0, 254, 255};

        public static string GetFileType(byte[] file, string fileName)
        {
            string fileType = "";

            if (string.IsNullOrWhiteSpace(fileName))
                return fileType;

            string extension = Path.GetExtension(fileName) == null
                ? string.Empty
                : Path.GetExtension(fileName).ToLower();

            if (file.Take(2).SequenceEqual(BMP))
                fileType = "bmp";
            else if (file.Take(8).SequenceEqual(DOC))
                fileType = GetDocFileType(extension);
            else if (file.Take(4).SequenceEqual(GIF))
                fileType = "gif";
            else if (file.Take(4).SequenceEqual(ICO))
                fileType = "ico";
            else if (file.Take(3).SequenceEqual(JPG))
                fileType = "jpeg";
            else if (file.Take(7).SequenceEqual(PDF))
                fileType = "pdf";
            else if (file.Take(16).SequenceEqual(PNG))
                fileType = "png";
            else if (file.Take(7).SequenceEqual(RAR))
                fileType = "rar";
            else if (file.Take(5).SequenceEqual(TIFF))
                fileType = "tiff";
            else if (file.Take(4).SequenceEqual(ZIP_DOCX))
                fileType = GetDocXFileType(extension);
            else if (file.Take(3).SequenceEqual(TXT_UTF8) || 
                     file.Take(2).SequenceEqual(TXT_UTF16BE) || 
                     file.Take(4).SequenceEqual(TXT_UTF32BE) ||
                     file.Take(2).SequenceEqual(TXT_UTF16LE))
                fileType = "txt";

            return string.IsNullOrEmpty(fileType) ? extension.Replace(".","") : fileType;
        }

        private static string GetDocFileType(string extension)
        {
            switch (extension)
            {
                case ".doc":
                    return "doc";
                case ".xls":
                    return "xls";
                case ".ppt":
                    return "ppt";
                default:
                    return "";
            }
        }

        private static string GetDocXFileType(string extension)
        {
            switch (extension)
            {
                case ".docx":
                    return "docx";
                case ".xlsx":
                    return "xlsx";
                case ".pptx":
                    return "pptx";
                case ".zip":
                    return "zip";
                default:
                    return "";
            }
        }
    }
}